<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('welcome');
    });
    
    
    Route::resource('rappels', '\App\Http\Controllers\ReminderController');
});
Route::get('/dashboard', function() {
    return view('dashboard');
});
Route::post('/check',  ['uses' => 'App\Http\Controllers\ReminderController@check', 'as' => 'checkReminder']); 
Route::post('/uncheck',  ['uses' => 'App\Http\Controllers\ReminderController@uncheck', 'as' => 'uncheckReminder']); 
Route::get('/list', ['uses' => 'App\Http\Controllers\ReminderController@list', 'as' => 'list']);
Route::get('list/export', ['uses' => 'App\Http\Controllers\RemindersExportController@export', 'as' => 'export']);
Route::get('/select', ['uses' => 'App\Http\Controllers\ReminderController@select', 'as' => 'select']); 
Route::get('/selectDose', ['uses' => 'App\Http\Controllers\ReminderController@selectDose', 'as' => 'selectDose']); 



