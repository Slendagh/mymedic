@extends ('layouts.layout')
@section('header')
@include('layouts.navigation')
@endsection
@section ('content')


<style>
    #create_new_reminder_link{
        --tw-text-opacity: 1;
        color:#059669;
    }
    #create_new_reminder_link:hover{
        --tw-text-opacity: 1;
        font-size: 1.1em;
        font-weight: 300;
        color: #047857;
    }
    .reminder-line{
        border: 2px solid #989491ad;
        border-radius: 4px;
        padding: 4px 6px;
        margin: 0px auto 10px;
        box-shadow: 0px 1px 1px #00000047;
        width: 100%;
        max-width: 500px;
        display:flex;
        flex-direction:column;
        overflow: hidden;
        height:auto;
        transition:all 0.3s ease-in-out;
        cursor: pointer;
    }
    .reminder-line.active, .reminder-line:hover{
        margin-top:-2px;
        margin-bottom:12px;
        box-shadow: 0px 3px 3px #FECACA;
        border: 2px solid #F87171;
    }
    .reminder-line>div{
        padding:4px 6px;
    }
    .reminder-header{

    }
    .reminder-body{
        height:0px;
        opacity:0;
        transition: all 0.3s ease-in-out;
        overflow: hidden;
    }
    .reminder-line.active .reminder-body{
        height:auto;
        opacity:1;
    }
    .checked{
        text-decoration : line-through;
    }
    .title{
        height: 100px;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    .list{
        margin: auto;
        width: 100%;
        max-width: 500px;
    }
    @media only screen and (max-width:600px){
        .list{
            max-width:350px;
            margin:auto;
        }
    }
</style>



    
    @if (session('status'))
        <div class="fade-message text-center bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session('status') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
        </div>
    @endif
<div class="my-4 flex flex-col items-center justify-center flex-column">
    @if($uncheckedReminders == false)
    <div id="title" class="m-1 mb-2 font-bold title" >C'est bon, vous êtes à jour !</div>
    @else
    <div id="title" class="m-1 mb-2 font-bold title" >Vos Rappels d'Aujourd'hui</div>
    @endif
    <div class ="list" id="uncheckedReminders">
        @foreach($reminders as $reminder)
            @if($reminder->reminderStatus != 'checked')
                @if($reminder->timesPerDay ==2)
                    <div class="reminder-line" id="{{$reminder->id}}/matin" onclick="openReminder({{$reminder->id}}+'/matin');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'matin');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - matin</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/soir" onclick="openReminder({{$reminder->id}}+'/soir');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'soir');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - soir</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                @elseif($reminder->timesPerDay == 3)
                    <div class="reminder-line" id="{{$reminder->id}}/matin" onclick="openReminder({{$reminder->id}}+'/matin');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'matin');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - matin</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/après-midi" onclick="openReminder({{$reminder->id}}+'/après-midi');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'après-midi');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - après-midi</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/soir" onclick="openReminder({{$reminder->id}}+'/soir');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'soir');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - soir</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                @elseif($reminder->timesPerDay ==4)
                    <div class="reminder-line" id="{{$reminder->id}}/matin" onclick="openReminder({{$reminder->id}}+'/matin');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'matin');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - matin</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/après-midi" onclick="openReminder({{$reminder->id}}+'/après-midi');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'après-midi');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - après-midi</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/soir" onclick="openReminder({{$reminder->id}}+'/soir');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'soir');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - soir</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                    <div class="reminder-line" id="{{$reminder->id}}/nuit" onclick="openReminder({{$reminder->id}}+'/nuit');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, 'nuit');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}} - nuit</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="reminder-line" id="{{$reminder->id}}/" onclick="openReminder({{$reminder->id}}+'/');">
                        <div class="reminder-header flex items-center justify-between">
                            <div class="flex flex-row items-center">
                                <input class="checkbox" type="checkbox" onclick="checkReminder({{$reminder->id}}, '');"><p class="font-medium mx-2">{{ $reminder->title ?? ''}}</p>
                            </div> 
                            <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                        </div>
                        <div class="reminder-body">
                            <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                            <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                            <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                            </form>
                        </div>
                    </div>
                @endif
            @endif
            <script>
                //at a certain time of day (how many times depends on number of time a medication needs to be taken per day)
                //push notification that contains the name and the logo of the medication to be taken
                function push_notification(){
                    /*var today = new Date();
                    if(today.toLocaleTimeString() == "10:00:00 AM")
                    {*/
                        Push.create("Prenez votre médicament!", {
                            Body:"{{ $reminder->title }}\n{{ $reminder->drug->name }}",
                            timeout:5000,
                        });
                    //}
                    console.log("{{ $reminder->title }}\n{{ $reminder->drug->name }}");
                }
            </script>
        @endforeach
        <div id="create_new_reminder_link"  class="title" style="text-align: center;"><a href="{{ route('rappels.create') }}">+ créer un nouveau rappel</a></div>
    </div>

    <!--checked Reminders-->
    <div class="list" id="checkedReminders">
        @foreach($reminders as $reminder)
            @if ($reminder->reminderStatus == 'checked')
            <div class="reminder-line {{$reminder->reminderStatus}}" id="{{$reminder->id}}/{{$reminder->time_in_day}}" onclick="openReminder({{$reminder->id}}+'/'+{{$reminder->time_in_day}});">
                <div class="reminder-header flex items-center justify-between">
                    <div class="flex flex-row items-center">
                        <input class="checkbox" type="checkbox" onclick="uncheckReminder({{$reminder->id}}, {{$reminder->time_in_day}});" checked><p class="font-medium mx-2">{{ $reminder->title ?? ''}} {{ $reminder->time_in_day ?? ''}}</p>
                    </div> 
                    <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a>
                </div>
                <div class="reminder-body">
                    <div class="test p-1.5 m-2">{{ $reminder->drug->name ?? ''}} {{ $reminder->drug->drug_dose ?? ''}} </div>
                    <div class="test p-1.5 m-2">{{ $reminder->notes ?? ''}}</div>
                    <form action="{{ route('rappels.destroy', $reminder->id) }}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button style="color:red;"><i style="color:red;" class="fas fa-trash"></i> Supprimer le Rappel</button>
                    </form>
                </div>
            </div>
            @endif
        @endforeach
    </div>
    <!-- end of checked Reminders-->

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script> 
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    function checkReminder(id, timeInDay){
        console.log(id)
        $.ajax({
            type: 'POST',  
            url: '/check', 
            data:  {'id':id, 'timeInDay':timeInDay},
            dataType: 'text',
            success: function(response) {
                console.log(response);
                var reminderItem = document.getElementById(id+'/'+timeInDay);
                if(response == "reminder is checked"){
                    reminderItem.classList.add('checked');
                    reminderItem.children[0].children[0].children[0].setAttribute('onclick','uncheckReminder('+id+'/'+timeInDay+')');
                    document.getElementById("checkedReminders").append(reminderItem);
                }else{
                    console.log("response is not correct");
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }

    function uncheckReminder(id, timeInDay){
        console.log(id)
        $.ajax({
            type: 'POST',  
            url: '/uncheck', 
            data:  {'id':id, 'timeInDay': timeInDay},
            dataType: 'text',
            success: function(response) {
                console.log(response);
                var reminderItem = document.getElementById(id);
                if(response == "reminder is unchecked"){
                    reminderItem.classList.remove('checked');
                    reminderItem.children[0].children[0].children[0].setAttribute('onclick','checkReminder('+id+')');
                    document.getElementById("uncheckedReminders").prepend(reminderItem);
                }else{
                    console.log(response);
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }

    function openReminder(id){
        document.getElementById(id).className += " active";
    }


    document.addEventListener("click", function(evt) {
        for(i=0; i<document.getElementsByClassName('reminder-line active').length; i++){
            var isClickInside = document.getElementsByClassName('reminder-line active')[i].contains(event.target);
            if (!isClickInside) {
                console.log('hi');
                document.getElementsByClassName('reminder-line active')[i].classList.remove("active");
            }
        }

        //is you click on checkbox, prevent from opening reminder
        for(i=0; i<document.getElementsByClassName('checkbox').length; i++){
            if(document.getElementsByClassName("checkbox")[i].contains(event.target)){
                var this_checkbox = document.getElementsByClassName('checkbox')[i];
                while(this_checkbox = this_checkbox.parentNode){
                    if(this_checkbox.classList.contains("active") == true){
                        this_checkbox.classList.remove("active");
                        break;
                    }
                }
            }
        }
        
        //if you click on edit, prevent from opening reminder
        for(i=0; i<document.getElementsByClassName('edit').length; i++){
            if(document.getElementsByClassName("edit")[i].contains(event.target)){
                var this_edit = document.getElementsByClassName('edit')[i];
                while(this_edit = this_edit.parentNode){
                    if(this_edit.classList.contains("active") == true){
                        this_edit.classList.remove("active");
                        break;
                    }
                }
            }
        }
        
    });

    $(document).ajaxStop(function(){
        var el = document.getElementById('uncheckedReminders');
        if (el.childNodes[0].className !== "reminder-line") {
            document.getElementById('title').innerText = "C'est bon, vous êtes à jour !";
        }else{
            document.getElementById('title').innerText = "Vos Rappels d'Aujourd'hui";
        }
    });

/*
    $(function(){
        setTimeout(function() {
            $('.fade-message').slideUp();
        }, 5000);
    });

    setInterval(push_notification, 1000 * 60 * 60)//call push function every hour*/
</script>

@endsection





