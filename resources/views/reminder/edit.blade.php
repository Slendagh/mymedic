@extends ('layouts.layout')
@section('header')
@include('layouts.navigation')
@endsection
@section ('content')

<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script>

<style>
  [x-cloak] {
    display: none;
  }

  .daysradio{
    width: 40%;
  }
  .cell{
      width:400px;
  }

  @media only screen and (max-width:600px){
        
        #form{
            width:400px;
        }
        .cell{
            width:320px;
        }
        .itemName{
            width:320px;
        }
    }
</style>
    <!--my code below-->
    
    <div class="flex items-center justify-center">
    
        <form id="form" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-2 mt-4" method="POST" action="{{route('rappels.update', $reminder->id)}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        @method('PUT')
            <br>
            <h1 class="block text-gray-700 font-bold mb-2 text-xl text-center">Modifier les informations du Médicament</h1>
            <br>
            <div class="mb-4 cell">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                    Nom du Médicament
                </label>
                <!--<input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    name="name" id="name" type="text" placeholder="Nom du Médicament" value="{{$reminder->drug->name}}" required>-->
                <select class="itemName form-control" style="width:500px;" name="name" id="name" onchange="selectDose();"></select>
            </div>
            <div class="mb-4 cell">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="dose">
                    Dose du Médicament
                </label>
                <!--<input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    name="dose" id="dose" type="text" placeholder="Dose du Médicament" value="{{$reminder->drug->drug_dose}}" required>--> 
                <select  class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Dose du Médicament"  name="dose" id="dose"></select>
            </div>
            <div class="mb-4 cell">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="title">
                    Titre du rappel <span style="color:gray">(optionnel)</span>
                </label>
                <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    name="title" id="title" type="text" placeholder="Titre du rapel" value="{{$reminder->title}}" required>
            </div>
                <!--date picker-->
            <div class="cell" x-data="app()" x-init="[initDate(), getNoOfDays()]" x-cloak>
                <div class="container mx-auto">
                    <div class="mb-5 w-64">
                        <label for="datepicker" class="font-bold mb-2 text-sm text-gray-700 block">Date de début <span style="color:gray">(optionnel)</span></label> 
                        <div class="relative">
                            <input type="hidden" name="start_date" x-ref="date">
                            <input 
                                type="text"
                                readonly
                                x-model="datepickerValue"
                                @click="showDatepicker = !showDatepicker"
                                @keydown.escape="showDatepicker = false"
                                class="w-full pl-4 pr-10 py-3 leading-none rounded-lg shadow-sm focus:outline-none focus:shadow-outline text-gray-600 font-medium"
                                placeholder="Sélectionner la date de début">
                            <div class="absolute top-0 right-0 px-3 py-2">
                                <svg class="h-6 w-6 text-gray-400"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"/>
                                </svg>
                            </div>
                            <div 
                                class="bg-white mt-12 rounded-lg shadow p-4 absolute top-0 left-0" 
                                style="width: 17rem" 
                                x-show.transition="showDatepicker"
                                @click.away="showDatepicker = false">
                                <div class="flex justify-between items-center mb-2">
                                    <div>
                                        <span x-text="MONTH_NAMES[month]" class="text-lg font-bold text-gray-800"></span>
                                        <span x-text="year" class="ml-1 text-lg text-gray-600 font-normal"></span>
                                    </div>
                                    <div>
                                        <button 
                                            type="button"
                                            class="transition ease-in-out duration-100 inline-flex cursor-pointer hover:bg-gray-200 p-1 rounded-full" 
                                            :class="{'cursor-not-allowed opacity-25': month == 0 }"
                                            :disabled="month == 0 ? true : false"
                                            @click="month--; getNoOfDays()">
                                            <svg class="h-6 w-6 text-gray-500 inline-flex"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"/>
                                            </svg>  
                                        </button>
                                        <button 
                                            type="button"
                                            class="transition ease-in-out duration-100 inline-flex cursor-pointer hover:bg-gray-200 p-1 rounded-full" 
                                            :class="{'cursor-not-allowed opacity-25': month == 11 }"
                                            :disabled="month == 11 ? true : false"
                                            @click="month++; getNoOfDays()">
                                            <svg class="h-6 w-6 text-gray-500 inline-flex"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"/>
                                            </svg>									  
                                        </button>
                                    </div>
                                </div>
                                <div class="flex flex-wrap mb-3 -mx-1">
                                    <template x-for="(day, index) in DAYS" :key="index">	
                                        <div style="width: 14.26%" class="px-1">
                                            <div
                                                x-text="day" 
                                                class="text-gray-800 font-medium text-center text-xs"></div>
                                        </div>
                                    </template>
                                </div>
                                <div class="flex flex-wrap -mx-1">
                                    <template x-for="blankday in blankdays">
                                        <div 
                                            style="width: 14.28%"
                                            class="text-center border p-1 border-transparent text-sm"	
                                        ></div>
                                    </template>	
                                    <template x-for="(date, dateIndex) in no_of_days" :key="dateIndex">	
                                        <div style="width: 14.28%" class="px-1 mb-1">
                                            <div
                                                @click="getDateValue(date)"
                                                x-text="date"
                                                class="cursor-pointer text-center text-sm leading-none rounded-full leading-loose transition ease-in-out duration-100"
                                                :class="{'bg-blue-500 text-white': isToday(date) == true, 'text-gray-700 hover:bg-blue-200': isToday(date) == false }"	
                                            ></div>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--date de fin-->
            <div class="cell" x-data="app()" x-init="[initDate(), getNoOfDays()]" x-cloak>
                <div class="container mx-auto">
                    <div class="mb-5 w-64">
                        <label for="datepicker" class="font-bold mb-2 text-gray-700 text-sm block">Date de fin <span style="color:gray">(optionnel)</span></label>
                        <div class="relative">
                            <input type="hidden" name="end_date" x-ref="date">
                            <input 
                                type="text"
                                readonly
                                x-model="datepickerValue"
                                @click="showDatepicker = !showDatepicker"
                                @keydown.escape="showDatepicker = false"
                                class="w-full pl-4 pr-10 py-3 leading-none rounded-lg shadow-sm focus:outline-none focus:shadow-outline text-gray-600 font-medium"
                                placeholder="Select date">
                            <div class="absolute top-0 right-0 px-3 py-2">
                                <svg class="h-6 w-6 text-gray-400"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"/>
                                </svg>
                            </div>
                            <div 
                                class="bg-white mt-12 rounded-lg shadow p-4 absolute top-0 left-0" 
                                style="width: 17rem" 
                                x-show.transition="showDatepicker"
                                @click.away="showDatepicker = false">
                                <div class="flex justify-between items-center mb-2">
                                    <div>
                                        <span x-text="MONTH_NAMES[month]" class="text-lg font-bold text-gray-800"></span>
                                        <span x-text="year" class="ml-1 text-lg text-gray-600 font-normal"></span>
                                    </div>
                                    <div>
                                        <button 
                                            type="button"
                                            class="transition ease-in-out duration-100 inline-flex cursor-pointer hover:bg-gray-200 p-1 rounded-full" 
                                            :class="{'cursor-not-allowed opacity-25': month == 0 }"
                                            :disabled="month == 0 ? true : false"
                                            @click="month--; getNoOfDays()">
                                            <svg class="h-6 w-6 text-gray-500 inline-flex"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"/>
                                            </svg>  
                                        </button>
                                        <button 
                                            type="button"
                                            class="transition ease-in-out duration-100 inline-flex cursor-pointer hover:bg-gray-200 p-1 rounded-full" 
                                            :class="{'cursor-not-allowed opacity-25': month == 11 }"
                                            :disabled="month == 11 ? true : false"
                                            @click="month++; getNoOfDays()">
                                            <svg class="h-6 w-6 text-gray-500 inline-flex"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"/>
                                            </svg>									  
                                        </button>
                                    </div>
                                </div>
                                <div class="flex flex-wrap mb-3 -mx-1">
                                    <template x-for="(day, index) in DAYS" :key="index">	
                                        <div style="width: 14.26%" class="px-1">
                                            <div
                                                x-text="day" 
                                                class="text-gray-800 font-medium text-center text-xs"></div>
                                        </div>
                                    </template>
                                </div>
                                <div class="flex flex-wrap -mx-1">
                                    <template x-for="blankday in blankdays">
                                        <div 
                                            style="width: 14.28%"
                                            class="text-center border p-1 border-transparent text-sm"	
                                        ></div>
                                    </template>	
                                    <template x-for="(date, dateIndex) in no_of_days" :key="dateIndex">	
                                        <div style="width: 14.28%" class="px-1 mb-1">
                                            <div
                                                @click="getDateValue(date)"
                                                x-text="date"
                                                class="cursor-pointer text-center text-sm leading-none rounded-full leading-loose transition ease-in-out duration-100"
                                                :class="{'bg-blue-500 text-white': isToday(date) == true, 'text-gray-700 hover:bg-blue-200': isToday(date) == false }"	
                                            ></div>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--fin datepicker-->
            <div class="mb-4 cell daysradio" style="padding-left: 10px; display: flex;flex-direction: row;justify-content: space-between;">
                <div>
                    <div>
                        <input type="checkbox" id="mon" name="days[]" class="weekday" value="mon">
                        <label for="mon">Lun</label>
                    </div>
                    <div>
                        <input type="checkbox" id="tue" name="days[]" class="weekday" value="tue">
                        <label for="tue">Mar</label>
                    </div>
                    <div>
                        <input type="checkbox" id="wed" name="days[]" class="weekday" value="wed" >
                        <label for="wed">Mer</label>
                    </div>
                    <div>
                        <input type="checkbox" id="thu" name="days[]" class="weekday" value="thu">
                        <label for="thu">Jeu</label>
                    </div>
                </div>
                <div>
                    <div>
                        <input type="checkbox" id="fr" name="days[]" class="weekday" value="fr">
                        <label for="fr">Ven</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sat" name="days[]" class="weekday" value="sat">
                        <label for="sat">Sam</label>
                    </div>
                    <div>
                        <input type="checkbox" id="sun" name="days[]" class="weekday" value="sun">
                        <label for="sun">Dim</label>
                    </div>
                    <div>
                        <input type="checkbox" onClick="everyDay(this)">
                        <label for="all">tous les jours</label>
                    </div>
                </div>
            </div>
            

            <div class="mb-4 cell">
                Nombre de fois par jour: 
                <select name="numberPerDay" id="number_per_day" required>
                    <?php for($i=1; $i<6; $i++){?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="mb-4 cell">
                <span>Chaque 
                <select name="everyNumberWeek" id="every_number_week">
                    <?php for($i=1; $i<5; $i++){?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php } ?>
                </select>
                    Semaine(s) </span>
            </div>

            <div class="mb-4 cell">

                <label class="block text-gray-700 text-sm font-bold mb-2" for="notes">
                    Notes
                </label>
                
                <textarea class="bshadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="notes" id="notes" type="text" placeholder="Ecrivez vos notes..." required>{{ $reminder->notes ?? ''}}</textarea>
            </div>

            
            <div class="flex items-center justify-between">
                <button id="submit"
                    class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit">
                    Enregistrer
                </button>
                    <div style="color:red;">{{ $error ?? '' }}</div>
            </div>

        </form>
        
    </div>
     
    <script>
      const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
          const DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

          function app() {
              return {
                  showDatepicker: false,
                  datepickerValue: '',

                  month: '',
                  year: '',
                  no_of_days: [],
                  blankdays: [],
                  days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],

                  initDate() {
                      let today = new Date();
                      this.month = today.getMonth();
                      this.year = today.getFullYear();
                      this.datepickerValue = new Date(this.year, this.month, today.getDate()).toDateString();
                      console.log("initDate function: "+ this.datepickerValue);
                  },

                  isToday(date) {
                      const today = new Date();
                      const d = new Date(this.year, this.month, date);

                      return today.toDateString() === d.toDateString() ? true : false;
                  },

                  getDateValue(date) {
                      let selectedDate = new Date(this.year, this.month, date);
                      this.datepickerValue = selectedDate.toDateString();

                      let correctMonthNumber = new Date(selectedDate).getMonth()+1;
                      this.$refs.date.value = selectedDate.getFullYear() +"-"+ ('0'+ correctMonthNumber).slice(-2) +"-"+ ('0' + selectedDate.getDate()).slice(-2);

                      console.log(this.$refs.date.value);

                      this.showDatepicker = false;
                  },

                  getNoOfDays() {
                      let daysInMonth = new Date(this.year, this.month + 1, 0).getDate();

                      // find where to start calendar day of week
                      let dayOfWeek = new Date(this.year, this.month).getDay();
                      let blankdaysArray = [];
                      for ( var i=1; i <= dayOfWeek; i++) {
                          blankdaysArray.push(i);
                      }

                      let daysArray = [];
                      for ( var i=1; i <= daysInMonth; i++) {
                          daysArray.push(i);
                      }

                      this.blankdays = blankdaysArray;
                      this.no_of_days = daysArray;
                  }
              }
          }

          $('.itemName').select2({
            placeholder: 'Nom du Médicament',
            width:'100%',
            tags:true,
            ajax: {
                url: '/select',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                    results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.name,
                            }
                        })
                    };
                },
                cache: true
            }
        });

       function selectDose(){
           var e = document.getElementsByName('name')[0];
           var selectedOption = e.options[e.selectedIndex].text;
            $.ajax({
            type: 'GET',  
            url: '/selectDose', 
            data:  {'drugName':selectedOption},
            dataType: 'json',
            success: function(response) {
                console.log(response);
                var htmlOptions = [];
                var select = document.getElementsByName('dose')[0];
                if(response.length > 0 ){
                    for(var i=0; i<response.length; i++ ) {
                        console.log(response[i]);
                        html = '<option value="' + response[i] + '">'+response[i]+'</option>';
                        htmlOptions[htmlOptions.length] = html;
                    }
                    // here you will empty the pre-existing data from you selectbox and will append the htmlOption created in the loop result
                    console.log(htmlOptions.join(''));
                    select.innerHTML = htmlOptions.join('');
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
       }
    </script>
            <!-- my code finish here-->
            
               
@endsection
    