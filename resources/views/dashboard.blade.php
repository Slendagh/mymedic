@extends ('layouts.layout')
@section('header')
@include('layouts.navigation')
@endsection
@section ('content')

<style>
  .bwata{
    padding:20px;
    margin-top:20px;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  #chart-pie{
    width:70%;
    margin-top:30px 
  }
  #chart-line{
    width:70%;
    margin-bottom:30px
  }
</style>

    <!-- Chart's container -->
    <div class="bwata">
      <div id="chart-line" style="height: 300px;"></div>
      <div id="chart-pie" style="height: 300px;"></div>
    </div>
    <!-- Charting library -->
<script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
<!-- Chartisan -->
<script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>
    <!-- Your application script -->
    <script>
      const chart = new Chartisan({
        el: '#chart-line',
        url: "@chart('user_activity_chart')",
        hooks: new ChartisanHooks()
            .colors(['rgb(255, 99, 132)','rgb(54, 162, 235)','rgb(255, 205, 86)', 'rgb(238, 130, 238)', 'rgb(255, 165, 0)', 'rgb(60, 179, 113)', 'rgba(255, 99, 71, 1)', 'rgb(0, 0, 255)'])
            .borderColors(['rgb(255, 99, 132)','rgb(54, 162, 235)','rgb(255, 205, 86)', 'rgb(238, 130, 238)', 'rgb(255, 165, 0)', 'rgb(60, 179, 113)', 'rgba(255, 99, 71, 1)', 'rgb(0, 0, 255)'])
            .beginAtZero()
            .responsive()
            .legend({position: 'bottom'})
            .title('Nombres mensuel de médicaments pris durant l\'année courante')
            .datasets([{ type: 'line', fill: false }]),
      });
      const chart2 = new Chartisan({
        el: '#chart-pie',
        url: "@chart('drug_chart')",
        hooks: new ChartisanHooks()
            .pieColors(['rgb(255, 99, 132)','rgb(54, 162, 235)','rgb(255, 205, 86)', 'rgb(238, 130, 238)', 'rgb(255, 165, 0)', 'rgb(60, 179, 113)', 'rgba(255, 99, 71, 1)', 'rgb(0, 0, 255)'])
            .legend({position: 'bottom'})
            .title('Fréquence relatives des médicaments prescrits')
            .datasets('pie'),
      });
    
    </script>
@endsection