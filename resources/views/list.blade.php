@extends ('layouts.layout')
@section('header')
@include('layouts.navigation')
@endsection
@section ('content')

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
<style>

/*Overrides for Tailwind CSS */
		
		/*Form fields*/
		.dataTables_wrapper select,
		.dataTables_wrapper .dataTables_filter input {
			color: #4a5568; 			/*text-gray-700*/
			padding-left: 1rem; 		/*pl-4*/
			padding-right: 1rem; 		/*pl-4*/
			padding-top: .5rem; 		/*pl-2*/
			padding-bottom: .5rem; 		/*pl-2*/
			line-height: 1.25; 			/*leading-tight*/
			border-width: 2px; 			/*border-2*/
			border-radius: .25rem; 		
			border-color: #edf2f7; 		/*border-gray-200*/
			background-color: #edf2f7; 	/*bg-gray-200*/
		}

		/*Row Hover*/
		table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
			background-color:#FCA5A5;	/*bg-red-300*/
		}
		
		/*Pagination Buttons*/
		.dataTables_wrapper .dataTables_paginate .paginate_button		{
			font-weight: 700;				/*font-bold*/
			border-radius: .25rem;			/*rounded*/
			border: 1px solid transparent;	/*border border-transparent*/
		}
		
		/*Pagination Buttons - Current selected */
		.dataTables_wrapper .dataTables_paginate .paginate_button.current	{
			color: #fff !important;				/*text-white*/
			box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06); 	/*shadow*/
			font-weight: 700;					/*font-bold*/
			border-radius: .25rem;				/*rounded*/
			background: #10B981 !important;		/*bg-green-500*/
			border: 1px solid transparent;		/*border border-transparent*/
		}

		/*Pagination Buttons - Hover */
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover		{
			color: #fff !important;				/*text-white*/
			box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);	 /*shadow*/
			font-weight: 700;					/*font-bold*/
			border-radius: .25rem;				/*rounded*/
			background: #059669 !important;		/*bg-green-600*/
			border: 1px solid transparent;		/*border border-transparent*/
		}
		
		/*Add padding to bottom border */
		table.dataTable.no-footer {
			border-bottom: 1px solid #e2e8f0;	/*border-b-1 border-gray-300*/
			margin-top: 0.75em;
			margin-bottom: 0.75em;
		}
		
		/*Change colour of responsive icon*/
		table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
			background-color: #047857 !important; /*bg-green-700*/
		}
    h1{
        font-size: 30px;
        padding:10px;
    }
    .title{
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        width: 100%;
        max-width:1550px;
    }
   
</style>



    @if (session('status'))
        <div class="fade-message text-center bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session('status') }}</span>
            <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg class="fill-current h-6 w-6 text-green-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
        </div>
    @endif
    @if(!$allReminders->isNotEmpty())
    
        <div class="flex justify-center items-center" style="height:50%">vous n'avez aucun médicament</div>
    @else
        <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2" style="height:100%; max-height:300px">
            
            <div class="title">
                <h1 class=" font-semibold text-green-500 break-normal px-2 py-8 text-xl md:text-2xl">Votre Liste de Médicaments</h1>
                <a href="{{ route('export') }}" class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" >Téléchargeable</a>
            </div>

            <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">
                <table id="table" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
					<thead>
                        <tr>
                            <th data-priority="1"> </th>
                            <th data-priority="1">Titre du rappel</th>
                            <th data-priority="2">Nom du Médicament</th>
                            <th data-priority="3">Dose du Médicament</th>
                            <th data-priority="4">Date de début</th>
                            <th data-priority="5">Date de fin</th>
                            <th data-priority="6">Jour de la semaine</th>
                            <th data-priority="7">Nombre de fois par jour</th>
                            <th data-priority="8">Frequence en semaine(s)</th>
                            <th data-priority="9">Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allReminders as $reminder)

                        <tr>
                            <td> <a href="{{ route('rappels.edit', $reminder->id) }}" class="edit"><i class="fas fa-pen"></i></a> </td>
                            <td>{{ $reminder->title ?? ''}}</td>
                            <td>{{ $reminder->drug->name ?? ''}}</td>
                            <td>{{ $reminder->drug->drug_dose ?? ''}}</td>
                            <td>{{ $reminder->start_date ?? ''}}</td>
                            <td>{{ $reminder->end_date ?? ''}}</td>
                            <td>
                                <?php
                                    $days=App\Models\Reminder::days($reminder->days);
                                    if(is_array($days)){
                                        for($k=0; $k<count($days); $k++){ 
                                            if(!next($days)){
                                                echo $days[$k];
                                            }else{
                                                echo $days[$k].", ";
                                            }
                                        }
                                    }else{
                                        echo $days;
                                    }
                                ?>
                            </td>
                            <td>{{ $reminder->timesPerDay ?? ''}}</td>
                            <td>{{ $reminder->every_x_weeks ?? ''}}</td>
                            <td>{{ $reminder->notes ?? ''}}</td>
                        </tr>
                
                        @endforeach
                    </tbody>
                </table>
            </div>
    @endif    


    <!-- jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		
    <!--Datatables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            
            var table = $('#table').DataTable( {
                    responsive: true,
                    "language": {
                        "url" : "//cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
                    }
                } )
                .columns.adjust()
                .responsive.recalc();
        } );

       
    
    </script>
@endsection

				
			
	

