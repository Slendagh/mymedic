@extends ('layouts.layout')

@section ('content')

<style>
    .welcome-section{
        height: 70%;
    }
    .link-guest{
        background-color:#ffc107;
        padding:7px;
        border-radius: 8px;
        margin-top:100px;
    }
    h1{
        font-size: 2.25rem;
    }
    h3{
        font-size: 1.35rem;
    }
    h3 a{
        border-bottom: 5px solid #ffc107;
    }
    h3 a:hover{
        --tw-text-opacity: 1;
        color: rgba(5, 150, 105, var(--tw-text-opacity));
        font-size: 1.1em;
        font-weight: 300;
    }
    nav{
        width:100%;
    }
</style>
        <div class="relative flex items-top justify-center bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
            @if (Route::has('login'))
                @auth
                    @include('layouts.navigation')
                @else
                <!--
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                -->
                @endauth
                </div>
            @endif
            </div>


        @guest
        <!--if not connected-->
        <div class="flex flex-col items-center justify-center welcome-section">
            <h1>Bienvenue à My medic</h1> 
            <h3>L'application web qui vous aide à prendre tous vos médicament à temps</h3>
            <h3 style="margin-bottom:20px">Pour de meilleures habitudes</h3>
            <div>
                <a class="link-guest" href="{{ route('login') }}">se connecter</a>
                <a class="link-guest" href="{{ route('register') }}">créer un compte</a>
            </div>
        </div>
        @endguest

        @auth
        <!-- if connected-->
        <div class="flex flex-col items-center justify-center welcome-section">
            <h3><a href="{{ route('rappels.index') }}">visitez vos rappels du jour</a> ou <a href="{{ route('list') }}">allez à la liste de tous vos médicaments</a></h1>
        </div>
        @endauth











        </div>
        
@endsection
   