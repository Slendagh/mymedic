<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugsMoroccoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs_morocco', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('code');
            $table->string('name');
            $table->string('DCI1');
            $table->string('dosage');
            $table->string('dosage_unit');
            $table->string('form');
            $table->string('presentation');
            $table->bigInteger('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs_mrorocco');
    }
}
