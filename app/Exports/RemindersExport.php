<?php

namespace App\Exports;
use App\Models\Reminder;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class RemindersExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $reminders = Reminder::where("user_id" , "=", auth()->user()->id)->with('drug')->get();
        $array =[];
            foreach($reminders as $reminder)
            {
                array_push($array, ['titre' => $reminder->title, 'nom du medicament'=> $reminder->drug->name, 'dose'=>$reminder->drug->drug_dose, 'date de debut'=>$reminder->start_date, 'date de fin'=>$reminder->end_date, 'jour de a=la semaine'=>$reminder->days, 'nombre' =>$reminder->timesPerDay, 'semaines'=>$reminder->every_x_weeks , 'notes'=> $reminder->notes ]);
            }
        $reminder_collection = collect($array);
        return $reminder_collection;
    }

    public function headings(): array
    {
        return [
            'titre du rappel',
            'nom du médicament',
            'dose du médicament',
            'date de début',
            'date de fin',
            'jour(s) de la semaine',
            'nombre de fois par jour',
            'Frequence en semaine(s)',
            'notes'
        ];
    }

    public function registerEvents() : array
    {
        return [
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->getStyle('A1:I1')->applyFromArray([
                    'font'=>[
                        'bold'=>true,
                    ]
                ]);
            }
        ];
    }
}
