<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class UserActivity extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $fillable = [
        'reminder_id',
        'state',
        'date',
        'created_at',
        'updated_at'
    ];

    public static function getLinerData($month, $drug_id)
    {
        $data = UserActivity::where([['state', '=', true], ['drug_id', '=', $drug_id], ['user_id', '=', auth()->user()->id]])->get();//get only user activities of one specific drug $drug
        $count = 0;
        
        for($k=0; $k<count($data); $k++)
        {
########need to check year here...
            if(\Carbon\Carbon::parse($data[$k]->date)->format('m') == $month)
            {
                $count +=1;//cont number of userActivity (drugs taken) in a given month
            }
        }
        return $count;
    }
}
