<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DrugMorocco extends Model
{
    use HasFactory;

    protected $table = 'drugs_morocco';

    protected $fillable = ['code', 'name', 'DCI1', 'dosage', 'dosage_unit', 'form', 'presentation', 'price'];
}
