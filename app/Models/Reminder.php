<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Drug;
use App\Models\User;
use Carbon\Carbon;

class Reminder extends Model
{
    use HasFactory;

    protected $table = 'reminders_entries';

    protected $fillable = [
        'title',
        'drug_id',
        'days',
        'timesPerDay',
        'every_x_weeks',
        'notes',
        'user_id',
        'LastTimeCompleted',
        'start_date',
        'end_date',
    ];
    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }

    public function activity()
    {
        $now = Carbon::now();
        $areChecked = UserActivity::where('reminder_id', '=', $this->id)->get();//orderBy created_at because we want to check the last added one
        if(!$areChecked->isEmpty()){
            for($i=0; $i<count($areChecked); $i++){
                $date = Carbon::parse($areChecked[$i]->date)->startOfDay();
                if( $areChecked[$i]->date==true && $date->eq($now->startOfDay()) ){
                    $this->reminderStatus = "checked"; 
                }
            }
        }else{
            $this->reminderStatus = "unchecked"; 
        }
    }

    public function getCreatedAtAttribute($value) 
    {      
        $timezone =  auth()->user()->timezone;
        return Carbon::parse($value)->timezone($timezone);
    }

    public function getLastTimeCompletedAttribute($value)
    {
        if($value == null){
            $value = $this->created_at;
            return $value;
        }
        $timezone =  auth()->user()->timezone;
        return Carbon::parse($value)->timezone($timezone);
    }

    public function getDateAttribute($value)
    {
        return $value;
    }
    public static function days($value)
    {
        $array='{"mon":"lundi", "tue": "mardi", "wed":"mercredi", "thu":"jeudi", "fr":"vendredi", "sat":"samedi", "sun":"dimance"}';
        $arrayObject = json_decode($array);
        $daysObject = json_decode($value);
        $result = [];
        foreach($daysObject as $key => $val)
        {
            foreach($arrayObject as $cle => $el)
            {
                if($val == $cle){
                   array_push($result, $el);
                }
            }
        }
        if(count($result) == 7){
            return "toute la semaine";
        }else{
            return $result;
        }
    }
}
