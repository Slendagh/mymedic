<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Drug extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'drug_dose'];

    public static function frequency($drug){
        //days
        //timesPerDay
        //every x weeks
        //end date - start date = 
        ##calculate number of days (end date, start date, every x weeks, days) 
        ##then calculate % relative frequency
        $reminders = Reminder::where([['user_id', '=', auth()->user()->id], ['drug_id', '=', $drug]])->get();
        for($i=0; $i<count($reminders); $i++){
            if($reminders[$i]->start_date == null ){
                $reminders[$i]->start_date = $reminders[$i]->created_at;
            }
            if($reminders[$i]->end_date == null){//if not defined then add default one year
                $reminders[$i]->end_date = Carbon::parse($reminders[$i]->start_date)->addYears();
            }
            $total_days = Carbon::parse($reminders[$i]->end_date)->diffInDays($reminders[$i]->start_date);
            $total_weeks = $total_days / 7;
            if($reminders[$i]->every_x_weeks >1){
                $total_weeks /= $reminders[$i]->every_x_weeks;// we need to represent total_days with weeks before calculating
                $total_days = $total_weeks * 7;
            }
            $decoded_days = json_decode($reminders[$i]->days);
            if(count($decoded_days) < 7){
                if($reminders[$i]->every_x_weeks  == 1){
                    $total_days = $total_days - ((7 - count($decoded_days)) * $total_weeks);// we need to represent total_days with months before calculating
                }else if($reminders[$i]->every_x_weeks  == 2){
                    $total_days = $total_days - ((7 - count($decoded_days)) * ($total_weeks/2));// we need to represent total_days with months before calculating
                }else if($reminders[$i]->every_x_weeks  == 3){
                    $total_days = $total_days - ((7 - count($decoded_days)) * ($total_weeks/3));// we need to represent total_days with months before calculating
                }
            }
        }
        return round($total_days);
    }

    
}
