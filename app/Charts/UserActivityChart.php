<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use App\Models\UserActivity;
use App\Models\User;
use App\Models\Drug;
use Illuminate\Support\Facades\Auth;

class UserActivityChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $drugsId = UserActivity::where('user_id', '=', Auth::id())->select('drug_id')->distinct()->get();
        $ddd = Chartisan::build()
            ->labels(['Janvier', 'février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']);

        for($i=0; $i<count($drugsId); $i++){
            $drug = Drug::find($drugsId[$i]);
            $ddd->dataset($drug[0]->name, [UserActivity::getLinerData('01', $drug[0]->id), UserActivity::getLinerData('02', $drug[0]->id), UserActivity::getLinerData('03', $drug[0]->id), UserActivity::getLinerData('04', $drug[0]->id), UserActivity::getLinerData('05', $drug[0]->id), UserActivity::getLinerData('06', $drug[0]->id),UserActivity::getLinerData('07', $drug[0]->id),UserActivity::getLinerData('08', $drug[0]->id),UserActivity::getLinerData('09', $drug[0]->id),UserActivity::getLinerData('10', $drug[0]->id),UserActivity::getLinerData('11',$drug[0]->id), UserActivity::getLinerData('12', $drug[0]->id)]);
        }
        return $ddd;
    }
}