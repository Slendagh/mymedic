<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use App\Models\Reminder;
use App\Models\Drug;

class DrugChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $array_drugFreq = [];
        $array_drugNames = [];
        $array_percntg = [];
        $reminders = Reminder::where('user_id', '=', auth()->user()->id)->with('drug')->get();
        $ddd = Chartisan::build();

        for($i=0; $i<count($reminders); $i++){
            $drugName = $reminders[$i]['drug']->name;
            $drugFreq= Drug::frequency($reminders[$i]->drug_id);
            array_push($array_drugNames, $drugName); 
            array_push($array_drugFreq, $drugFreq);
        }

        $sum = array_sum($array_drugFreq);
        for($k=0; $k<count($array_drugFreq); $k++){
            $percentage = ($array_drugFreq[$k] / $sum ) * 100;
            array_push($array_percntg, round($percentage));
        }
        $ddd->labels($array_drugNames);
        $ddd->dataset('', $array_percntg);
        return $ddd;
    }
}