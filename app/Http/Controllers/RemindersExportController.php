<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\RemindersExport;

class RemindersExportController extends Controller
{
    public function export(){
        return Excel::download(new RemindersExport, 'mes_rappels.xlsx');
    }
}
