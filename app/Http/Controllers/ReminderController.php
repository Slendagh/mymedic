<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reminder;
use App\Models\Drug;
use App\Models\UserActivity;
use Carbon\Carbon;
use WeakReference;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $reminders = Reminder::where('user_id', '=', $user->id)->with('drug')->get();
        $todaysReminders=[];
        $now = Carbon::now($user->timezone);

        foreach($reminders as $reminder){
            $start_date = Carbon::parse($reminder->start_date);
            $end_date = Carbon::parse($reminder->end_date);
            
            //check if the interval between start and end date correspond to today
            if(($reminder->start_date == null || $start_date->lessThanOrEqualTo($now) == true) && ($reminder->end_date == null || $end_date->greaterThanOrEqualTo($now) == true))
            {
                //checking if the scheduled week correspond to the current week
                $every_x_weeks = $reminder->every_x_weeks;
                $lastTimeReminded = $reminder->LastTimeCompleted;
                //see if its the right week 
                ////take the start of week of lastTimeCompleted date
                $startOfWeek_LastTimeCompleted = $lastTimeReminded->startOfWeek()->format('Y-m-d H:i');
                /////compare it to now 
                $weeksDifference = Carbon::parse($startOfWeek_LastTimeCompleted)->diffInWeeks($now);
                if($every_x_weeks == 1 || $reminder->LastTimeCompleted->startOfWeek()->equalTo($now->startOfWeek()) )
                {
                    $daysObject = json_decode($reminder->days);
                    foreach($daysObject as $key => $value)
                    {
                        if(strpos(strtolower(Carbon::now($user->timezone)->format('l')), $value) !== false)
                        {
                            array_push($todaysReminders, $reminder);
                            //update the last time reminded variable if not updated already
                            if($lastTimeReminded->toDateString() !== $now->toDateString()){
                                $rem = Reminder::find($reminder->id);
                                $rem->LastTimeCompleted = Carbon::now();
                                $rem->save();//???
                            }
                        }
                    }
                }
                elseif($every_x_weeks > 1)
                {
                    if($weeksDifference == $every_x_weeks)
                    {
                        $daysObject = json_decode($reminder->days);
                        foreach($daysObject as $key => $value)
                        {
                            if(strpos(strtolower(Carbon::now($user->timezone)->format('l')), $value) !== false)
                            {
                                array_push($todaysReminders, $reminder);
                                //update the last time reminded variable if not updated already
                                if($lastTimeReminded->toDateString() !== $now->toDateString()){
                                    $rem = Reminder::find($reminder->id);
                                    $rem->LastTimeCompleted = Carbon::now();
                                    $rem->save();//???
                                }
                            }
                        }
                    }else{
                        //smth needs to go here or we do smth about reminders that are not checked by end of day because if we miss a reminder that comes every 2 weeks or more, we will lose it in the lasttimereminded... we wont have referene anymore
                        //or maybe not becuase of the code above ?
                    }
                }
                
            }
            $reminder->activity();
        }
        $i=0;
        $uncheckedReminders = true;
        for($k=0; $k<count($todaysReminders); $k++){
            if($todaysReminders[$k]->reminderStatus == 'checked'){
                $i+=1;
            }
        }
        if($i == count($todaysReminders)){
            $uncheckedReminders = false;
        }
        return view('reminder.index', ['reminders' => $todaysReminders, 'uncheckedReminders'=> $uncheckedReminders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reminder.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            /*'name' => 'required|string|max:255',*/
        ]);

        if(!$request->days){
            $error = "Veuillez cocher les jours correspondants";
            return view('reminder.create', ['error' => $error]);
        }

        $reminder = new Reminder;
        $drug = new Drug;
        $error=""; 

        $drug->name = $request->name;
        $drug->drug_dose = $request->dose;
        
        if(!$reminder->title){
            $reminder->title = "Prendre ".$drug->name." ".$drug->drug_dose;
        }else{
            $reminder->title = $request->title;
        }

        $reminder->user_id =  auth()->user()->id;
        $reminder->days = json_encode($request->days);
        $reminder->timesPerDay= $request->numberPerDay;
        $reminder->every_x_weeks = $request->everyNumberWeek;
        $reminder->notes = $request->notes;
        $reminder->start_date = $request->start_date;
        $reminder->end_date = $request->end_date;
        $drug->firstOrCreate(['name'=>$drug->name], ['drug_dose'=>$drug->drug_dose]);
        $id= Drug::where(['name' => $drug->name], ['drug_dose' => $drug->drug_dose])->pluck('id')->last();

        if(Reminder::where('title', '=', $reminder->title)->count() > 0){
            $error = "Ce titre du rappel existe déja";
            return view('reminder.create', ['error' => $error]);
        }else{
            if($id){
                $reminder->drug_id = $id;
            }else{
                $reminder->drug_id = $drug->id;
            }
            $reminder->save();
            return redirect()->action([ReminderController::class, 'index'])->with('status', 'le rappel a été bien enregistrer!');//send this message to controller index then send it to view dashboard
        }
        

    }

    /**
     * Display the specified resource.
     *
     *@param  int  $id
    * @return \Illuminate\Http\Response
     */
    /*public function check(Request $request){
        $now = Carbon::now();
        $today = true;
        $reminder = Reminder::find($request->id);
        $userActivity = new UserActivity;
        $isChecked = UserActivity::where('reminder_id', '=', $request->id)->orderBy('date', 'desc')->first();//orderBy created_at because we want to check the last added one
        if($isChecked !== null){
            $date = Carbon::parse($isChecked->date)->startOfDay();
            if( $date->eq($now->startOfDay()) ){
                $activity = UserActivity::find($isChecked->id);
                $activity->delete();
                return "reminder is unchecked";
            }else{
                $today = false;
            }
        }
        if($isChecked == null ){
            $userActivity->reminder_id = $request->id;
            $userActivity->state = 1;
            $userActivity->user_id = auth()->user()->id;
            $userActivity->drug_id = $reminder->drug_id;
            $userActivity->date = Carbon::now();
            $userActivity->save();
            return "reminder is checked";
        }
        if($today == false){
            $userActivity->state = 1;
            $userActivity->user_id = auth()->user()->id;
            $userActivity->drug_id = $reminder->drug_id;
            $userActivity->date = Carbon::now();
            $userActivity->reminder_id = $request->id;
            $userActivity->save();
            return "reminder is checked";
        }
    }*/

    public function check(Request $request){
        $reminder = Reminder::find($request->id);
        $userActivity = new UserActivity;
        $userActivity->reminder_id = $request->id;
        $userActivity->state = 1;
        $userActivity->user_id = auth()->user()->id;
        $userActivity->drug_id = $reminder->drug_id;
        $userActivity->date = Carbon::now();
        $userActivity->time_in_day = $request->timeInDay;
        $userActivity->save();
        return "reminder is checked";
    }

    public function uncheck(Request $request){//would be better if ajax send timesPerDay too for accuracy in the line bellow
        $now = Carbon::now();
        $isChecked = UserActivity::where('reminder_id', '=', $request->id)->orderBy('date', 'desc')->first();//add "where timesPerDay = ..." 
        if($isChecked !== null){
            $date = Carbon::parse($isChecked->date)->startOfDay();
            if( $date->eq($now->startOfDay()) ){
                $activity = UserActivity::find($isChecked->id);
                $activity->delete();
                return "reminder is unchecked";
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("this is show function");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reminder = Reminder::where('id', '=', $id)->first();
        return view('reminder.edit', ['reminder' => $reminder]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reminder = Reminder::find($id);
        $reminder->title = $request->title;
        $reminder->days = json_encode($request->days);
        $reminder->timesPerDay= $request->numberPerDay;
        $reminder->every_x_weeks = $request->everyNumberWeek;
        $reminder->notes = $request->notes;
        $reminder->start_date = $request->start_date;
        $reminder->end_date = $request->end_date;

        if(!$request->days){
            $error = "Veuillez cocher les jours correspondants";
            return view('reminder.edit', ['reminder'=> $reminder, 'error' => $error]);
        }

        $drug = new Drug;
        $drug->name = $request->name;
        $drug->drug_dose = $request->dose;
        $ourDrug = $drug->firstOrCreate(['name'=>$drug->name], ['drug_dose'=>$drug->drug_dose]);
        
        $reminder->drug_id = $ourDrug->id;

        if(Reminder::where('title', '=', $reminder->title)->count() > 0 && Reminder::where('id', '=', $id)->pluck('title')->first() != $reminder->title){
            $error = "Ce titre du rappel existe déja";
            return view('reminder.edit', ['reminder'=> $reminder, 'error' => $error]);
        }else{
            $reminder->save();
            return redirect()->action([ReminderController::class, 'index'])->with('status', 'le rappel a été bien modifier!');//send this message to controller index then send it to view dashboard
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reminder::destroy($id);
        return redirect()->action([ReminderController::class, 'index'])->with('status', 'le rappel a été bien supprimer!');
    }

    public function dashboad(){
        //return view('dashboard');
    }

    public function list(){
        $allReminders = Reminder::where("user_id" , "=", auth()->user()->id)->with('drug')->get();
        return view('list', ['allReminders' => $allReminders]);
    }

    public function export(){

        dd("exporting");
    }

    public function select(Request $request){
        $data = [];
        if($request->has('q')){
            $search = strtoupper($request->q);
            $data = Drug::where('name','LIKE',"%$search%")->get();
        }
        return response()->json($data);
    }

    public function selectDose(Request $request){
        $data = [];
        $drugName = strtoupper($request->drugName);
        $data = Drug::where('name', '=', $drugName)->pluck('drug_dose')->toArray();
        return response()->json($data);
    }
}
